package models;

public class RegistrationModel {
    private RegistrationModel account;
    private String nameField;
    private String emailField;
    private String passwordField;
    private String passwordField2;

    public RegistrationModel getAccount() {
        return account;
    }

    public void setAccount(RegistrationModel account) {
        this.account = account;
    }

    public String getNameField() {
        return nameField;
    }

    public void setNameField(String nameField) {
        this.nameField = nameField;
    }

    public String getEmailField() {
        return emailField;
    }

    public void setEmailField(String emailField) {
        this.emailField = emailField;
    }

    public String getPasswordField() {
        return passwordField;
    }

    public void setPasswordField(String passwordField) {
        this.passwordField = passwordField;
    }

    public String getPasswordField2() {
        return passwordField2;
    }

    public void setPasswordField2(String passwordField2) {
        this.passwordField2 = passwordField2;
    }
}


