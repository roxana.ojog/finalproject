package models;

public class LoginModel {
    private LoginModel account;
    private String emailField;
    private String passwordField;

    public LoginModel getAccount() {
        return account;
    }

    public void setAccount(LoginModel account) {
        this.account = account;
    }

    public String getEmailField() {
        return emailField;
    }

    public void setEmailField(String emailField) {
        this.emailField = emailField;
    }

    public String getPasswordField() {
        return passwordField;
    }

    public void setPasswordField(String passwordField) {
        this.passwordField = passwordField;
    }
}

