package models;

public class SubscribeModel {
    private SubscribeModel subscribe;
    private String emailAddress;

    public SubscribeModel getSubscribe() {
        return subscribe;
    }

    public void setSubscribe(SubscribeModel subscribe) {
        this.subscribe = subscribe;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }


}
