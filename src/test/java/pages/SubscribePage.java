package pages;

import models.SubscribeModel;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class SubscribePage {
    public SubscribePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @FindBy(how = How.ID, using = "alert_email")
    private WebElement subscribeEmail;

    @FindBy(how = How.XPATH, using = "//button[@type='submit']")
    private WebElement subscribeButton;

    public void subscribe(SubscribeModel accountData) {
        subscribeEmail.clear();
        subscribeEmail.sendKeys((CharSequence) accountData.getSubscribe());

        clickToSubscribe();
    }

    public boolean clickToSubscribe() {
        subscribeButton.click();
        return true;

    }
}

