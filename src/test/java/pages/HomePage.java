package pages;

import models.SubscribeModel;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class HomePage {
    public HomePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @FindBy(how = How.XPATH, using = "//a[@id='login_open']")
    private WebElement login;

    @FindBy(how = How.XPATH, using = "//ul[@class='nav']//a[contains(@href, 'page=user')]")
    private WebElement myAccount;

    @FindBy(how = How.XPATH, using = "//ul[@class='nav']//a[contains(@href, 'page=register')]")
    private WebElement registerAccount;

    @FindBy(how = How.XPATH, using = "//div[@class='wrapper']//a[contains(@href, 'page=contact')]")
    private WebElement contact;

    @FindBy(how = How.XPATH, using = "//div[@class='box location']//a[contains(@href, 'Region=782166')]")
    private WebElement region;

    @FindBy(how = How.ID, using = "alert_email")
    private WebElement subscribeEmail;

    @FindBy(how = How.XPATH, using = "//button[@type='submit']")
    private WebElement subscribeButton;


    public void clickToLogin() {
        login.click();
    }

    public void clickToRegister() {
        registerAccount.click();
    }

    public boolean checkMyAccountVisibility() {
        myAccount.isDisplayed();
        return true;

    }

    public void clickToContact() {
        contact.click();
    }

    public void clickToRegion() {
        region.click();
    }

    public void clickToSubscribe() {

        subscribeButton.click();
    }


    }



