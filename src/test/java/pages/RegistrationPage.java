package pages;


import models.RegistrationModel;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class RegistrationPage {
    public RegistrationPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @FindBy(how = How.ID, using = "s_name")
    private WebElement nameField;

    @FindBy(how = How.ID, using = "s_email")
    private WebElement emailField;

    @FindBy(how = How.ID, using = "s_password")
    private WebElement passwordField;

    @FindBy(how = How.ID, using = "s_password2")
    private WebElement passwordField2;

    @FindBy(how = How.XPATH, using = "//button[@type='submit']")
    private WebElement createButton;

    @FindBy (how = How.XPATH, using = "//div[@class='flashmessage flashmessage-ok']")
    private WebElement succesfullRegistration;


    public void registration(RegistrationModel accountData) {
        nameField.clear();
        nameField.sendKeys(accountData.getNameField());

        emailField.clear();
        emailField.sendKeys(accountData.getEmailField());

        passwordField.clear();
        passwordField.sendKeys(accountData.getPasswordField());

        passwordField2.clear();
        passwordField2.sendKeys(accountData.getPasswordField2());

        createButton.click();
    }
    public boolean validateSuccessfulRegistration() {
        succesfullRegistration.isDisplayed();
        return true;
    }
}
