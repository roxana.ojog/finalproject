package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class ContactNegative {

    public ContactNegative(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @FindBy(how = How.ID, using = "yourName")
    private WebElement nameField;

    @FindBy(how = How.ID, using = "yourEmail")
    private WebElement emailAddress;

    @FindBy(how = How.ID, using = "subject")
    private WebElement subjectField;

    @FindBy(how = How.ID, using = "message")
    private WebElement messageField;

    @FindBy(how = How.XPATH, using = "//*[@id='main']/div/div[2]/form/div[5]/div/button")
    private WebElement sendButton;

}