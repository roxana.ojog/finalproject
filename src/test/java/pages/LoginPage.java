package pages;

import models.LoginModel;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
    public LoginPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @FindBy(how = How.ID, using = "email")
    private WebElement emailField;

    @FindBy(how = How.ID, using = "password")
    private WebElement passwordField;

    @FindBy(how = How.XPATH, using = "//button[@type='submit']")
    private WebElement loginButton;

    @FindBy(how = How.XPATH, using = "//div[@id='flashmessage'][1]")
    private WebElement errorEmailAddress;

    @FindBy(how = How.XPATH, using = "//div[@id='flashmessage'][2]")
    private WebElement errorPassword;



    public void login (LoginModel accountData) {
        emailField.clear();
        emailField.sendKeys(accountData.getEmailField());

        passwordField.clear();
        passwordField.sendKeys(accountData.getPasswordField());

        loginButton.click();
    }
    public boolean validateNegativePassword() {
        errorPassword.isDisplayed();
        return true;}

        public boolean validateNegativeEmail() {
        errorEmailAddress.isDisplayed();
        return true;
        }


}
