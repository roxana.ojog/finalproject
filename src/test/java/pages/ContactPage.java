package pages;

import models.ContactModel;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class ContactPage {
    public ContactPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @FindBy(how = How.ID, using = "yourName")
    private WebElement nameField;

    @FindBy(how = How.ID, using = "yourEmail")
    private WebElement emailAddress;

    @FindBy(how = How.ID, using = "subject")
    private WebElement subjectField;

    @FindBy(how = How.ID, using = "message")
    private WebElement messageField;

    @FindBy(how = How.XPATH, using = "//*[@id=\"main\"]/div/div[2]/form/div[5]/div/button")
    private WebElement sendButton;

    @FindBy(how = How.XPATH, using = "//ul[@id='error_list']//label[@for='yourEmail']")
    private WebElement errorEmailAddress;

    @FindBy(how = How.XPATH, using = "//ul[@id='error_list']//label[@for='message']")
    private WebElement errorMessageField;



    public void contact(ContactModel accountData) {
        nameField.clear();
        nameField.sendKeys(accountData.getNameField());

        emailAddress.clear();
        emailAddress.sendKeys(accountData.getEmailAddress());

        subjectField.clear();
        subjectField.sendKeys(accountData.getSubjectField());

        messageField.clear();
        messageField.sendKeys(accountData.getMessageField());

        sendButton.click();


    }

    public boolean validateSuccessfulContact() {
        sendButton.isDisplayed();
        return true;
    }

        public boolean validateMessageField() {
            errorMessageField.isDisplayed();
            return true;}

        public boolean validateEmailAddress() {
            errorEmailAddress.isDisplayed();
            return true;
        }
    }


