package tests;

import base.Browser;
import base.WebBrowser;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.concurrent.TimeUnit;



public class  BaseTest {
    WebDriver driver;
    String BASE_URL =  "http://siit.epizy.com/osc/";

    @BeforeMethod
    public void initDriver()throws InterruptedException {
        driver = WebBrowser.getDriver(Browser.CHROME);
        driver.manage().window().fullscreen();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.get(BASE_URL);
    }
    @AfterMethod
    public void cleanUp() {
        this.driver.quit();
    }

    protected File[] getListOfFilesFromResources(String directoryName) throws UnsupportedEncodingException {
        ClassLoader classLoader = getClass().getClassLoader();
        File directory = new File(URLDecoder.decode(classLoader.getResource(directoryName).getPath(), "UTF-8"));
        File[] files = directory.listFiles();
        System.out.println("Found " + files.length + " files in " + directoryName + " folder");
        return files;
    }



}
