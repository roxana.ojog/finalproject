package tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import models.ContactModel;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.ContactPage;
import pages.HomePage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class ContactTest extends BaseTest {
    private HomePage homePage;
    private ContactPage contactPage;

    private void initPage(){
        homePage = new HomePage(this.driver);
        contactPage = new ContactPage(this.driver);

    }

    //data provider

    @DataProvider(name = "JSONContactData")
    public Iterator<Object[]> jsonDataProviderCollection() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Collection<Object[]> dp = new ArrayList<>();
        File[] files = getListOfFilesFromResources("jsonContact");

        for (File f : files) {
            ContactModel m = mapper.readValue(f, ContactModel.class);
            dp.add(new Object[]{m});
        }
        return dp.iterator();
    }
    // data provider negative

    @DataProvider(name = "JSONContactNegativeData")
    public Iterator<Object[]> jsonDataProviderCollectionNegative() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Collection<Object[]> dp = new ArrayList<>();
        File[] files = getListOfFilesFromResources("jsonContactNegative");

        for (File f : files) {
            ContactModel m = mapper.readValue(f, ContactModel.class);
            dp.add(new Object[]{m});
        }
        return dp.iterator();
    }


// tests
    @Test(dataProvider = "JSONContactData" )
    public void ContactHappyFlow(ContactModel contactModel){
    initPage();
    homePage.clickToContact();
    contactPage.contact(contactModel.getContact());

    Assert.assertTrue(contactPage.validateSuccessfulContact());
}

// test negative
@Test(dataProvider = "JSONContactNegativeData" )
    public void ContactNegativeFlow(ContactModel contactModel){
    initPage();
    homePage.clickToContact();
    contactPage.contact(contactModel.getContact());

    Assert.assertTrue(contactPage.validateMessageField());
    Assert.assertTrue(contactPage.validateEmailAddress());
}

}
