package tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import models.LoginModel;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.LoginPage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class LoginTest extends BaseTest {
    private HomePage homePage;
    private LoginPage loginPage;

    private void initPage(){
        homePage = new HomePage(this.driver);
        loginPage = new LoginPage(this.driver);
    }

    //data provider

    @DataProvider(name = "JSONLoginData")
    public Iterator<Object[]> jsonDataProviderCollection() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Collection<Object[]> dp = new ArrayList<>();
        File[] files = getListOfFilesFromResources("jsonLogin");

        for (File f : files) {
            LoginModel m = mapper.readValue(f, LoginModel.class);
            dp.add(new Object[]{m});
        }
        return dp.iterator();
    }

 // negative
    @DataProvider(name = "JSONLoginNegativeData")
    public Iterator<Object[]> jsonDataProviderCollectionNegative() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Collection<Object[]> dp = new ArrayList<>();
        File[] files = getListOfFilesFromResources("jsonLoginNegative");

        for (File f : files) {
            LoginModel m = mapper.readValue(f, LoginModel.class);
            dp.add(new Object[]{m});
        }
        return dp.iterator();
    }

    //test

    @Test(dataProvider = "JSONLoginData" )
    public void LoginHappyFlow(LoginModel loginModel){
        initPage();
        homePage.clickToLogin();
        loginPage.login(loginModel.getAccount());

        Assert.assertTrue(homePage.checkMyAccountVisibility());
    }

    @Test(dataProvider = "JSONLoginNegativeData" )
    public void LoginNegativeFlow(LoginModel loginModel){
        initPage();
        homePage.clickToLogin();
        loginPage.login(loginModel.getAccount());

        Assert.assertTrue(loginPage.validateNegativeEmail());
        Assert.assertTrue(loginPage.validateNegativePassword());
    }

}
