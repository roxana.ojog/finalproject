package tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import models.LoginModel;
import models.SubscribeModel;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.SubscribePage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class SubscribeTest extends BaseTest {
    private HomePage homePage;
    private SubscribePage subscribePage;

    private void initPage(){
        homePage = new HomePage(this.driver);
        subscribePage = new SubscribePage(this.driver);

    }


    // data provider
    @DataProvider(name = "JSONSubscribeData")
    public Iterator<Object[]> jsonDataProviderCollection() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Collection<Object[]> dp = new ArrayList<>();
        File[] files = getListOfFilesFromResources("jsonSubscribe");

        for (File f : files) {
            SubscribeModel m = mapper.readValue(f, SubscribeModel.class);
            dp.add(new Object[]{m});
        }
        return dp.iterator();
    }

    //test
    @Test(dataProvider = "JSONSubscribeData" )
    public void SubscribeHappyFlow(SubscribeModel subscribeModel){
        initPage();
        homePage.clickToRegion();
        subscribePage.subscribe(subscribeModel.getSubscribe());

        Assert.assertTrue(subscribePage.clickToSubscribe());
    }


}