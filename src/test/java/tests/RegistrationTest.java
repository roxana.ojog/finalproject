package tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import models.RegistrationModel;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.RegistrationPage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class RegistrationTest extends BaseTest {
    private HomePage homePage;
    private RegistrationPage registrationPage;

    private void initPage() {
        homePage = new HomePage(this.driver);
        registrationPage = new RegistrationPage(this.driver);
    }
    @DataProvider(name = "JSONRegistrationData")
    public Iterator<Object[]> jsonDataProviderCollection() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Collection<Object[]> dp = new ArrayList<>();
        File[] files = getListOfFilesFromResources("jsonRegistration");

        for (File f : files) {
            RegistrationModel m = mapper.readValue(f, RegistrationModel.class);
            dp.add(new Object[]{m});
        }
        return dp.iterator();
    }
    @Test(dataProvider = "JSONRegistrationData")
    public void RegistrationHappyFlow(RegistrationModel registrationModel){
        initPage();
        homePage.clickToRegister();
        registrationPage.registration(registrationModel.getAccount());

        Assert.assertTrue(registrationPage.validateSuccessfulRegistration());
    }

}



